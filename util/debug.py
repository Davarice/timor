"""
Module dedicated to handling console messages
"""

from re import compile

from colorama import Back, Fore, Style


_color = {
    "INFO": Fore.BLACK + Style.BRIGHT,
    "DEBUG": "",
    "WARN": Fore.YELLOW,
    "ERROR": Fore.RED + Style.BRIGHT,
    "CRIT": Back.RED + Fore.YELLOW + Style.BRIGHT,
}
_queued = []

long_url = compile(r"http[ds]?://\S{100,}")


def send(prefix: str, msg: str, ex: Exception = None):
    prefix = prefix.upper()
    if prefix not in _color:
        raise ValueError("Invalid debug class '{}'".format(prefix))
    print(_color[prefix] + prefix + ": " + msg + Style.RESET_ALL)
    if ex:
        print("  - Caught Exception: " + long_url.sub("[LONG URL]", str(ex)))


def send_later(prefix: str, msg: str, ex: Exception = None):
    _queued.append((prefix, msg, ex))


def dump():
    for msg in _queued:
        send(*msg)

    _queued.clear()
