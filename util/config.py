"""
Module dedicated to loading the config file and exposing it to other modules
"""

from sys import exit

import oyaml

from util.debug import send
from util.paths import get_file, get_path


cfg = "config.yml"
file = {}

expected_options = ["client-id", "interim", "refresh", "following", "default_profile"]


sound_suppress = False


def get(opt: str, default=None):
    """
    Follow a path through the datastructure to retrieve a value, or a default.
    This is honestly just an overengineered recursive dict.get...
    """
    # Split the path
    path = opt.split("/")
    cur = file
    failure = {}
    while path:
        # Follow the path down, with a custom default (because $default may not
        #     implement .get(), which we need)
        if cur is None:
            cur = failure
        else:
            cur = cur.get(path.pop(0), failure)
    if cur is failure:
        # If the target value was not found, send back the REAL default
        return default
    else:
        return cur


def load(path=None):
    global cfg
    path = path or cfg
    cfg = path
    concrete = get_path(path)
    send("info", "Loading configuration file at '{}'".format(concrete))
    refresh()


def refresh():
    global file
    concrete = get_path(cfg)
    try:
        with get_file(concrete) as fh:
            try:
                file = oyaml.safe_load(fh)
            except Exception as ex:
                send("crit", "Unreadable Config File", ex)
                if not file:
                    exit(2)
    except Exception as ex:
        send("crit", "Inaccessible Config File", ex)
        if not file:
            exit(2)


def find_missing(lookfor=None) -> list:
    if lookfor is None:
        lookfor = expected_options
    failure = {}
    return [opt for opt in lookfor if get(opt, failure) is failure]


def enforce():
    missing = find_missing()
    if missing:
        send(
            "error",
            "Invalid Configuration: The following required config options are missing:\n    {}".format(
                ", ".join(missing)
            ),
        )
        exit(2)
