"""
Module dedicated to saving timestamped records of live channels
"""

from json import dump

from util import config
from util.paths import get_file, get_path
from util.debug import send, send_later


logpath = None
livefile = None


def load(path=None, fallback_allowed=False):
    global livefile
    livefile = config.file["file_live"]
    if livefile:
        livefile = get_path(livefile)

    global logpath
    default = config.file["logfile"]
    logpath = get_path(path or default)

    try:
        # Quickly open a file at the configured location to test that it is possible
        get_file(logpath, "a").close()
    except Exception as ex:
        send("error", "Logfile at '{}' could not be accessed.".format(path), ex)
        # Failed to open logfile. Are we able to fallback to the default?
        if fallback_allowed and path and path != default:
            # Yes? Cool, try to load the default. Only recurse once, though.
            send("info", "Trying default logfile location from config...")
            load(default, fallback_allowed=False)
        else:
            # No? Nothing we can do then. Output an error and make it shiny.
            send("crit", "This session has nowhere to be logged.")
            logpath = None
    else:
        send("info", "Using logfile at '{}'".format(logpath))


def record_page(data):
    """
    Open logfile to append and write logs in human-legible english
    """
    if not logpath:
        send_later("warn", "No logging is taking place.")
        return

    show = [e for e in data.entries if e.live or e.up or e.dn]
    try:
        if show:
            new = "TIMOR Log Output: " + data.time.strftime("%x %X %Z")
            for e in show:
                new += "\n" + config.file["logs"][e.live].format(
                    e=e, uptime=e.uptime(data.time)
                )
            new += "\n" * 2
            with logpath.open("a") as file:
                file.write(new)

            if livefile:
                export = {
                    e.aname: dict(
                        game=e.data.get("stream", {}).get("game", None),
                        title=e.title,
                        viewers=e.data.get("stream", {}).get("viewer_count", 0),
                    )
                    for e in show
                    if e.live
                }
                with livefile.open("w") as file:
                    dump(export, file, indent=2)
                    # for e in show:
                    #     file.write(e.name + "\n")
    except Exception as ex2:
        send_later("warn", "Logfile write failed.", ex2)
