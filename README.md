[![Black](https://img.shields.io/badge/Codestyle-Black-000000.svg)](https://github.com/ambv/black)
[![Python](https://img.shields.io/badge/Python-3.7-1f425f.svg)](https://www.python.org/)
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

# Timor

**T**witch **I**nterface/**MON**itor, but **MOR**e. Python utility succeeding my old shell script from 2015, Timon.

Timon started out as a very basic list that would periodically fetch the stati of a list of channels on Twitch.tv. It went along with another Bash utility of mine, one I called Quickstreamer, which served simply as a CLI wrapper for the now-defunct [Livestreamer](https://github.com/chrippa/livestreamer), which lives on as [Streamlink](https://github.com/streamlink/streamlink).

As time went on these tools became increasingly bloated with feature creep, and after I installed a [Pi-Hole](https://github.com/pi-hole/pi-hole), I realized just how many API requests I was generating. This is an attempt to consolidate the two tools into one, significantly more streamlined, CLI tool.

## Usage

```bash
$ python3 /path/to/timor/run.py [OPTIONS]
```

##### Options

Option|Effect|
---:|:---
`-h`, `--help`                  | Print help text
`-c PATH`, `--config=PATH`      | Use a different configuration file
`-l PATH`, `--logfile=PATH`     | Use a different logfile
`--fallback`                    | Allow falling back to the config logfile if the above fails
`-q`, `--quiet`                 | Do not perform alert audio playback

## Setup and Config

The configuration file should be placed in the root directory of this repository, as `config.yml`. I have included `config_example.yml` to serve as a basis for the formatting, and included two of my favorite channels as well.

The config file is written in basic [YAML](https://en.wikipedia.org/wiki/YAML), as an Associative Array, and must contain the following fields:

- `client-id`: A string of the identification key for the app registered with Twitch. Previously, I included a default ID for Timor, but it has come to my attention that Twitch does rate limiting based on Client IDs, so you should register one [here](https://dev.twitch.tv/console/apps/create) as described on [this page](https://dev.twitch.tv/docs/authentication#registration).

- `logfile`: A string representation of the location on disk of the file to be used for logs. By default takes the form of a POSIX path, but hopefully [Pathlib](https://docs.python.org/3/library/pathlib.html) will allow the use of either POSIX or Windows path formats. If this is invalid, no logging will be done, but the program will not refuse to continue.

- `interim`: Either an Int or Float. Number of seconds to wait between Refreshes.

- `refresh`: An Integer. The number of Refreshes to perform on a page before performing another Update. **NOTE:** An *Update* is the operation wherein a **new Query** is sent and more recent information is fetched from Twitch. A *Refresh* is simply a refresh of the display to update any relative times, such as the uptime of a channel.

- `following`: Should be filled by another Array. Each key in this Array should be the username of a Twitch channel. The value associated with each username should be *yet another* Array. This final layer of Array should include the following fields:

  * `alert`: array: Same values as the global Alert setting, minus the Filepath. Defines overrides to beep or not beep specifically for this channel.

  * `alias`: string: An alternate label or name for the channel. Will be displayed in parentheses after the channel name proper in the list.

  * `important`: (boolean): Whether this channel is considered to be Priority. Priority channels will always be shown in the list, even if they are offline.

  * `record`: (boolean|array): A set of conditions under which this channel should be automatically recorded. If the value is `true`, the channel will *always* be recorded; if it is `false`, it will *never* be recorded. If the value is not Boolean, it must be an Array, with strings for values of specific keys:

    + `game`: A Regular Expression that will be matched against the Game name, OR a Game ID.

    + `title`: A Regular Expression that will be matched against the Stream Title.

    + `datetime`: A Regular Expression that will be matched against the date and time in ISO format. For recording at specific times.

    + `weekday`: A String of digits, representing integer days of the week on which this channel will be recorded. For example, `"1247"` will record this channel whenever it is live on Mondays, Tuesdays, Thursdays, and Sundays.

- `default_profile`: Another Array. Its format should be identical to that of `following` (see above). This setting is used as a default/fallback, for channels being displayed that have not been configured.

## Control Flow

- `run.py` - Entry point of the tool. Control passes directly down to classes of `core.py`.

- `core.py` - Centerpiece of the tool. Runs the loop and determines when to refresh. Control path **splits** down to `monitor` and `util` packages.

  * `monitor/api.py` - Communicates with Twitch at the request of `core.py`, returns information about important channels, and references `util/config.py` to determine what channels matter.

  * `monitor/fakecolor.py` - Provides nothing but a mock object to imitate the Colorama objects, when no actual color is desired. Contains no functionality.

  * `monitor/output.py` - Formats data that is to be displayed. Receives information from `core.py`, and cross-references it with `util/config.py` to determine what channels to display.

  * `monitor/record.py` - Performs livestream recording functionality, and keeps track of child processes. Opens the command given in the configuration, passing to it as an argument the filename given in the configuration, with timestamp inserted via `strftime()`.

  * `util/config.py` - Loads and exposes configuration file to the rest of the codebase. Referenced by almost everything.

  * `util/debug.py` - Provides a unified output interface for other modules to print debug and error messages.

  * `util/logs.py` - A kind of secondary output module. Receives information from `core.py`, and saves timestamped records to a logfile on the hard disk. References `util/config.py` for specifics such as logfile location/format, and references `util/paths.py` for access to the files.

  * `util/paths.py` - Provides a unified interface for other modules to access files on the hard drive.
