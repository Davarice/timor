"""Module dedicated to handling and printing query results."""

from datetime import datetime as dt, timedelta
from functools import partial
from re import compile, IGNORECASE
from shutil import get_terminal_size
from string import Template
from subprocess import run
from typing import List

from blessings import Terminal
from colorama import init, Cursor, Fore, Style
from playsound import playsound
import pytz

from monitor.fakecolor import Fake
from monitor.record import clean, is_recording, lenrec, recordings
from util import config
from util.debug import send, send_later
from util.paths import get_path


init()

T = Terminal()
uncolor = partial(
    compile(
        r"\x1b("
        r"(\[\??\d+[hl])|"
        r"([=<>a-kzNM78])|"
        r"([()][a-b0-2])|"
        r"(\[\d{0,2}[ma-dgkjqi])|"
        r"(\[\d+;\d+[hfy]?)|"
        r"(\[;?[hf])|"
        r"(#[3-68])|"
        r"([01356]n)|"
        r"(O[mlnp-z]?)|"
        r"(/Z)|"
        r"(\d+)|"
        r"(\[\?\d;\d0c)|"
        r"(\d;\dR))",
        flags=IGNORECASE,
    ).sub,
    "",
)


try:
    localtime = pytz.timezone(config.file["timezone"])
    send("info", f"Using timezone <{localtime}>")
except Exception as ex:
    send("warn", "Failed to set configured timezone. Falling back to UTC.", ex)
    localtime = pytz.utc


def cut(line: str, delta: int, num: int = 5) -> str:
    if delta >= 0:
        return line[: -(delta + num + 1)] + "…" + line[-num:]
    else:
        return line


def hour_min_sec(sec: float, precision: int = 3) -> str:
    m, s = divmod(int(sec), 60)
    # h, m = divmod(m, 60)
    # return ":".join(format(s, "0>2") for s in (h, m, s)[:precision])
    hms = (*divmod(m, 60), s)
    return ":".join(format(s, "0>2") for s in hms[:precision])


def utc_from(tstamp: str) -> dt:
    return dt.fromisoformat(tstamp[:-1]).replace(tzinfo=pytz.utc)


locali_e = lambda tstamp: localtime.normalize(tstamp.astimezone(localtime))
dtnow = lambda: locali_e(dt.now(tz=pytz.utc))


class Entry:
    def __init__(self, data: dict, old: dict, live, up, dn, update: dict, last=None):
        # Process and organize data that is static and independent;
        #   However, leave gaps to be filled with time, meta, etc.
        self.data = data
        self.live = live
        self.check = self.data.get("config", {}).get("alert", {})

        self.previous = last
        if last:
            last.previous = None

        self.up = up
        self.dn = dn
        new_title = False
        new_game = False

        if old:
            new_game = (old.get("stream") or {}).get("game") != (
                data.get("stream") or {}
            ).get("game")
            new_title = (old.get("stream") or {}).get("title") != (
                data.get("stream") or {}
            ).get("title")

            # Fill out this form detailing whether this Entry has changed.
            #   However, do NOT overwrite answers provided by other Entries.
            #   Additionally, only fill in a True if the per-channel config has
            #   not forbidden it.
            update["on_start"] = update["on_start"] or (
                up and self.check.get("on_start", True)
            )
            update["on_stop"] = update["on_stop"] or (
                dn and self.check.get("on_stop", True)
            )
            if not up and not dn:
                update["on_game_change"] = update["on_game_change"] or (
                    new_game and self.check.get("on_game_change", True)
                )
                update["on_title_change"] = update["on_title_change"] or (
                    new_title and self.check.get("on_title_change", True)
                )

        try:
            stream = data.get("stream", {})
            def do(cpath: str):
                cmd = config.get(cpath)
                if cmd:
                    return run(
                        [Template(w).substitute(stream) for w in cmd],
                        capture_output=True,
                    )

            if up or new_game:
                do("updates/game")
            if up or new_title:
                do("updates/title")
            if up or new_game or new_title:
                do("updates/either")
            if up or (new_game and new_title):
                do("updates/both")
        except:
            pass

        try:
            views = data["stream"]["viewer_count"]
        except (KeyError, TypeError):
            views = 0

        # Initialize an icon that precedes line 2, if line 2 will be shown.
        self.trend = T.bright_blue(
            format(views, config.get("display/delta_format", "<+3"))
        )
        # self.trend = T.bright_blue("⭓+0")

        if live:
            # Channel is live; Word will be green and (by default) "ONLINE!".
            status = (
                "{Style.BRIGHT}"
                + ("{Fore.CYAN}" if up else "{Fore.GREEN}")
                # + (
                #     config.get("display/status/rec", "xERRORx")
                #     if is_recording(self.name)
                #     else config.get("display/status/on", "xERRORx")
                # )
                + "${ON}{Fore.WHITE}"
            )
            if old and old.get("stream"):
                # Old data is provided; Also make a trend comparison.
                v_delta = views - old["stream"]["viewer_count"]
                dstr = format(v_delta, config.get("display/delta_format", "<+3"))
                if v_delta > 0:
                    # Viewers have gone up.
                    self.trend = T.green(dstr)
                    # self.trend = T.bright_green(config.get("display/trend/up") + dstr)
                elif v_delta < 0:
                    # Viewers have gone down.
                    self.trend = T.bright_red(dstr)
                    # self.trend = T.bright_red(config.get("display/trend/dn") + dstr)
                else:
                    # Viewers did not change.
                    self.trend = T.bright_yellow(dstr)
                    # self.trend = T.bright_yellow(
                    #     config.get("display/trend/hold") + dstr
                    # )
        else:
            # Channel is NOT live; Word will be red and (by default) "Offline".
            # Additionally, the trend icon will not change; This lets it serve
            #   an extra role as a "newly live" symbol automatically.
            status = (
                "{Style.NORMAL}"
                + ("{Fore.CYAN}" if dn else "{Fore.RED}")
                + config.get("display/status/off", "xERRORx")
                + "{Fore.BLACK}"
                + "{Style.BRIGHT}"
            )

        # These are the two halves of the first line of an entry, split apart at
        #   the gap to be filled by "........." based on width of longest name
        self.line1_1 = (
            "{Style.RESET_ALL}{Style.NORMAL}"
            + self.aname
            + "{Fore.BLACK}{Style.BRIGHT}"
        )
        self.line1_2 = (
            status
            + config.file["display"]["divide"]
            + ("{Fore.CYAN}" if new_game else "{Fore.BLUE}")
            + ("{Style.NORMAL}" if not live else "")
            + "${GAME}"
            # + self.game
            # # + "{Style.RESET_ALL}"
        )
        if live:
            # IF the channel is live, there will be a second line. This is that
            #   line, split apart at the uptime, which can only be evaluated
            #   immediately before the line is to be printed.
            type_effect = {"live": "{Style.BRIGHT}", None: "{Style.NORMAL}"}
            self.line2_1 = (
                # f"{T.green('(')}"
                f"{T.bright_cyan(f'{views:<5}')} {self.trend} "
                # f"{T.green('/')} "
            )
            self.line2_2 = (
                # f"{T.green(')')} "
                " # "
                + type_effect.get(self.data["stream"].get("type"), type_effect[None])
                + ("{Fore.CYAN}" if new_title else "{Fore.GREEN}")
                + "${STATUS}"
                # + self.title.replace("{", "{{{{").replace("}", "}}}}")
            )
        else:
            # Otherwise make them blank. These should never be called, but JIC.
            self.line2_1 = ""
            self.line2_2 = ""

    def render(self, now: dt, padlen: int) -> List[str]:
        # Put the first part of the line down. Then, add a number of pad characters
        #   equal to the total amount of padding minus the length of this name.
        #   Then, add the rest of the line.
        out = [
            # "{line:.<{pad}}".format(line=self.line1_1, pad=padlen)
            self.line1_1
            + str((config.get("display/pad", ".")) * (padlen - len(self.aname)))
            + self.line1_2
        ]
        if self.live:
            # If the channel is live, add a second line, detailing viewer count,
            #   stream uptime, and the title of the stream.
            out.append(self.line2_1 + T.bright_green(self.uptime(now)) + self.line2_2)
        elif self.dn:
            # If the channel has just gone offline, say so on the second line.
            out.append(T.bright_red("Channel Offline"))
        d = dict(
            GAME=self.game,
            STATUS=self.title.replace("{", "{{").replace("}", "}}"),
            ON=config.get("display/status/rec", "xERRORx")
            if is_recording(self.name)
            else config.get("display/status/on", "xERRORx"),
        )
        return [Template(line).safe_substitute(**d) for line in out]

    def uptime(self, now: dt) -> str:
        if not self.live:
            return "??:??"
        else:
            return hour_min_sec(
                (now - utc_from(self.data["stream"]["started_at"])).total_seconds(), 2
            )

    @property
    def aname(self) -> str:
        dname = self.name
        alias = config.get(f"following/{dname.lower()}/alias")
        return f"{dname} ({alias})" if alias else dname

    @property
    def name(self) -> str:
        return (
            self.data["user"]["display_name"]
            if "user" in self.data and "display_name" in self.data["user"]
            else "<ERROR>"
        )

    @property
    def game(self) -> str:
        if self.data["stream"]:
            return self.data["stream"].get("game", config.get("display/no_game"))
        else:
            return config.get("display/no_game")

    @property
    def title(self) -> str:
        if self.data["stream"]:
            return self.data["stream"]["title"].strip()
        else:
            return config.get("display/no_title")


class Page:
    """Build a legible display of the information contained in $new, using the
        contents of $old as a reference point to find and highlight changes and
        differences.
    """

    def __init__(self, new: dict, old: dict, lastpage=None):
        self.shown = False
        self.entries = []
        self.time = new.pop("TIME")
        self.refresh = self.time + timedelta(
            seconds=(config.get("interim") * config.get("refresh"))
        )

        self.previous = lastpage
        if lastpage:
            lastpage.previous = None

        self.beep = False
        # This Dict will be passed to every Entry and be updated with changes.
        changes = {
            "on_start": False,
            "on_stop": False,
            "on_game_change": False,
            "on_title_change": False,
        }

        # Go through all names in $new.
        for name, e_new in new.items():
            e_old = old.get(name, {})

            # Check whether the channel is or just was live, or is "important".
            is_live = bool(e_new.get("stream"))
            was_live = bool(e_old.get("stream"))

            if is_live or was_live or e_new.get("config", {}).get("important"):
                # Yes? Channel will be printed.
                newly_online = is_live and not was_live  # Just went live
                newly_offline = was_live and not is_live  # Just went offline

                # # TODO: Get previous version of this entry
                # last_entry = None
                # if self.previous:
                #     try:
                #         last_entry = self.previous.entries ...

                e = Entry(e_new, e_old, is_live, newly_online, newly_offline, changes)
                self.entries.append(e)

        # Check the changes against the config to find out whether we should
        #   output an alert.
        alerts = config.get("alert")
        for change, changed in changes.items():
            if changed and alerts.get(change):
                self.beep = True
                break

        self.width = max([len(e.aname) for e in self.entries]) + 5

    def render(self, width, quiet) -> List[str]:
        """
        Render every Entry into a list, apply Header and Footer, and return list
        """
        now = dtnow()
        minute, second = divmod(abs((self.refresh - now).seconds) + 1, 60)
        # out = (e.render(now, self.width) for e in self.entries)

        header = (
            "{Style.BRIGHT}"
            + (f"{{Fore.RED}}🔴{lenrec()} " if recordings else "")
            + "{Fore.BLUE}"
            + "Data from {} / Stale: {:>3} / Refresh: {} ({}):".format(
                # Date/Time this data was retrieved:
                "{Fore.GREEN}"
                + locali_e(self.time).strftime("%x T %X")
                + "{Fore.BLUE}",
                # Number of seconds since retrieval:
                "{Fore.GREEN}"
                + str(int((now - self.time).total_seconds())).center(3)
                + "s{Fore.BLUE}",
                # Approx. time of next data refresh:
                "{Fore.GREEN}" + locali_e(self.refresh).strftime("%X") + "{Fore.BLUE}",
                # Approx. time TO next data refresh:
                "{Fore.GREEN}T"
                + ("-" if self.refresh > now else "+")
                + f"{minute}:{second:0>2}"
                + "{Fore.BLUE}",
            )
        )
        if quiet:
            footer = T.bright_blue(format("! !! UPDATE !! !", "=^50"))
        else:
            footer = T.bright_blue("=" * 50)
        out = [
            header,
            *("\n".join(e.render(now, self.width)) for e in self.entries),
            footer,
        ]

        if config.get("color"):
            colored = [line.format(Fore=Fore, Style=Style) for line in out]
            # # raw = [line.format(Fore=Fake, Style=Fake) for line in out]
            # final = []
            #
            # for i, (line_r, line_c) in enumerate(zip(map(uncolor, colored), colored)):
            #     diff = len(line_r) - width
            #     final.append(cut(line_c, diff) if diff >= 0 else line_c)

            final = colored
            # final = [
            #     cut(line_c, len(line_r) - width)
            #     for line_c, line_r in zip(colored, map(uncolor, colored))
            # ]

        else:
            final = [
                cut(line.format(Fore=Fake, Style=Fake), len(line) - width)
                for line in out
            ]
            # for i, line in enumerate(final):
            #     diff = len(line) - width
            #     if diff >= 0:
            #         final[i] = cut(line, diff)

        return final

    def display(self):
        clean()
        width = get_terminal_size()[0]
        out = "\n".join(self.render(width, config.sound_suppress and self.beep))

        if not self.shown:
            # For the first refresh, clear the terminal and start anew.
            print("\x1b[2J\x1b[H" + out + Style.RESET_ALL)
            self.shown = True
            soundpath = config.get("alert/path")
            if self.beep and soundpath and not config.sound_suppress:
                try:
                    sound = get_path(soundpath)
                    if sound.is_file():
                        playsound(str(sound))
                    else:
                        send_later(
                            "error", f"Update alert sound not found at '{sound}'"
                        )
                except Exception as e:
                    send_later("error", "Failed to play sound.", e)
        else:
            # For subsequent refreshes, just reset to the top of the page.
            print(
                (Cursor.POS() + " " * (width * len(out)))
                if config.get("full_redraw")
                else "" + Cursor.POS() + out + Style.RESET_ALL
            )
