"""Module dedicated to interfacing with the Twitch API."""

from asyncio import sleep
from collections import defaultdict
from datetime import datetime as dt
from http.client import RemoteDisconnected
from pathlib import Path
from typing import Any, Dict, Iterator, Optional, Sequence, TypeVar, Union

from pytz import utc
from requests import Response
from requests_oauthlib import OAuth2Session
from requests_toolbelt.adapters import host_header_ssl

from util import config
from util.debug import send


api_addr: str = ""
api_domain: str = "api.twitch.tv"
api_url: str = "https://{}/helix/"

games_cache: Dict[int, str] = {}
updated: Optional[dt] = None

page_structure = Dict[str, Union[dt, Dict[str, Union[dict, None, str]]]]

sess = OAuth2Session(
    config.get("client-id"),
    auto_refresh_url="https://id.twitch.tv/oauth2/token",
    auto_refresh_kwargs={"client_secret": config.get("oauth")},
)
sess.mount("https://", host_header_ssl.HostHeaderSSLAdapter())
sess.headers.update(
    {
        "Client-ID": config.get("client-id"),
        # "host": api_domain,
    }
)

_V = TypeVar("_V")


def update_credentials():
    send("info", "Getting Auth info...")
    sess.client_id = config.get("client-id")

    resp = sess.post(
        "https://id.twitch.tv/oauth2/token",
        {
            "client_id": sess.client_id,
            "client_secret": config.get("oauth"),
            "grant_type": "client_credentials",
        },
    )

    if resp.ok:
        sess.token = {
            "access_token": resp.json()["access_token"],
            "token_type": "Bearer",
        }
    else:
        resp.raise_for_status()


update_credentials()


def chunk(
    targets: Sequence[_V], csize: int = 100, final: bool = True
) -> Iterator[Sequence[_V]]:
    """Return values from a Sequence in chunks up to a certain size."""
    while len(targets) > csize:
        yield targets[:csize]
        targets = targets[csize:]

    if final:
        yield targets


def find_api(_now: dt = None):
    # global api_addr, updated
    # now = now or dt.now(tz=utc)
    # if (
    #     not api_addr
    #     or not updated
    #     or (now - updated).total_seconds() >= config.get("ip_cache_time", 3600)
    # ):
    #     api_addr = gethostbyname(api_domain)
    #     updated = now
    #     send(
    #         "info",
    #         "Cached address:\n    {} -> {} @ {}".format(api_domain, api_addr, now),
    #     )

    # return api_url.format(api_addr)
    return api_url.format(api_domain)


async def request(url, repeat=3) -> Response:
    """Send a Query to a specified URL. Read the Status Code of the Response and
        potentially raise an Exception if the Query cannot be made successfully.
    """
    e = None
    r = None
    for i in range(3):
        try:
            r = sess.get(url)
        except RemoteDisconnected as e:
            await sleep(1)
            continue
        else:
            break

    if e and not r:
        raise e

    with r as response:
        # send(
        #     "info",
        #     "{} API 'Points' remaining.".format(
        #         response.headers.get("Ratelimit-Remaining", "Unknown")
        #     ),
        # )
        if not response.ok:
            if repeat > 0:
                send(
                    "warn",
                    "Received code {status} ({error}: {message}), retrying in"
                    " one second...".format(**response.json()),
                )
                await sleep(1)

                if response.status_code == 401:
                    update_credentials()

                return await request(url, repeat - 1)
            elif response.status_code == 429:
                raise RuntimeError("Rate Limited by the Twitch API.")
            else:
                raise RuntimeError(
                    "Request failed with Status Code {} ({!r}).".format(
                        response.status_code, response.reason
                    )
                )
        else:
            return response


def make_url(qtype: str, prefix: str, logins: Sequence[str], *, now: dt = None) -> str:
    """Construct a Query URL based on a Query Type, a Prefix for each Query, and
        a List of Keys to query.

    :param str qtype: Type of Query. "streams", "users", etc.
    :param str prefix: String to be prepended to each Query String in the List.
    :param str logins: A List of Queries to be made. Typically usernames.
    :param dt now: Current Timestamp.
    :return: A constructed Query URL that will return channel information.
    """
    send("info", f"Querying {qtype!r}...")
    names = "&".join(f"{prefix}={login}" for login in logins)
    return f"{find_api(now)}{qtype}?{names}"


async def set_games(streams: list):
    """The data returned from Twitch includes Game IDs, but no names. The user
        requires game names. Go through the data and resolve IDs into names,
        using cached data when possible and otherwise sending a query, and
        insert the game names into the data.
    This is done in-place.
    """
    to_change = defaultdict(list)

    # Go through the list of live channels and check the game ID.
    for channel in streams:
        game_id = channel.get("game_id")
        if not game_id:
            continue
        elif game_id in games_cache:
            # If the ID is cached, set the game name.
            channel["game"] = games_cache[game_id]
        else:
            # If not, make a note to fetch it, and to come back here later.
            to_change[game_id].append(channel)
        # elif game_id in to_change:
        #     # If not, make a note to fetch it, and to come back here later.
        #     to_change[game_id].append(channel)
        # else:
        #     to_change[game_id] = [channel]

    # Now, fetch the details of all the games that were not found in the cache.
    if to_change:
        games_fetched = (await request(make_url("games", "id", to_change))).json()[
            "data"
        ]

        # Go through the list of games.
        for game in games_fetched:
            game_id = game.get("id")
            if game_id and "name" in game:
                # We got information from the Server.
                game_name = game["name"]
            else:
                # We got no data for this ID.
                game_name = config.get("display/no_game")

            # Cache the game so we do not have to look it up again.
            games_cache[game_id] = game_name

            # Go to every channel with this game and update it now that we know
            #   the name.
            channels = to_change.get(game_id, [])
            for channel in channels:
                channel["game"] = game["name"]


async def get_page() -> page_structure:
    """Query the Twitch API and get information about all channels of interest.
        Then, organize the information and package it all into a convenient
        structure. Put a timestamp at the top and include pointers into the
        config, and return it.
    """
    following: Dict[str, Any] = config.get("following", {}).copy()
    tempfile = config.get("file_temp")

    # Assign the configured Default Profile to any channels that have been
    #   "temporarily followed", e.g. opened directly.
    if tempfile and (path := Path(tempfile)).exists():
        # noinspection PyUnboundLocalVariable
        with path.open("r") as file:
            for temp in set(file.readlines()):
                if (t2 := temp.strip()) not in following:
                    following[t2] = config.get("default_profile", {}).copy()

    # A request can only contain 100 targets, so it may need to be split up.
    reqs = list(chunk(list(following), 100))

    now = dt.now(tz=utc)
    users = []
    streams = []
    for req in reqs:
        r1 = await request(make_url("streams", "user_login", req, now=now))
        r2 = await request(make_url("users", "login", req, now=now))

        streams += r1.json()["data"]
        users += r2.json()["data"]

    await set_games(streams)

    data = {"TIME": now}
    for user in users:
        name = user.get("login", "").lower()
        if name:
            data[name] = {
                "config": following.get(name, None) or {},
                "stream": None,
                "user": user,
            }
    for stream in streams:
        name = stream.get("user_name", "").lower()
        data.get(name, {})["stream"] = stream

    return data
