"""Module for recording streams."""

from asyncio import gather, get_running_loop, Task, wait_for
from asyncio.subprocess import create_subprocess_shell, DEVNULL, Process
from datetime import datetime as dt
from pathlib import Path
from re import fullmatch
from typing import Dict, Tuple

from util import config
from util.debug import send
from util.paths import get_path


recordings: Dict[str, Tuple[Process, Task]] = {}
STREAM_URL: str = "https://www.twitch.tv/{}"


def clean():
    for channel, (proc, task) in recordings.copy().items():
        if task.done() and proc.returncode is not None:
            del recordings[channel]


def is_recording(channel: str) -> bool:
    # clean()
    return channel.lower() in recordings


def lenrec() -> int:
    # clean()
    return len(recordings)


kill_all = lambda: gather(
    *(stop_recording(channel) for channel in recordings),
    return_exceptions=True,
)


def should_record(data: dict) -> bool:
    """Determine whether the config file indicates to record this channel."""
    if not data.get("stream"):
        return False

    setting = data.get("config", {}).get("record", False)
    if not setting:
        return False
    elif setting is True:
        return True

    elif isinstance(setting, dict):
        if "game" in setting:
            x = data["stream"].get("game") or data["stream"].get("game_id")
            if setting["game"] == x or fullmatch(str(setting["game"]), str(x)):
                return True

        if "title" in setting:
            x = data["stream"].get("title")
            if setting["title"] == x or fullmatch(str(setting["title"]), str(x)):
                return True

        now = dt.now()

        if "datetime" in setting:
            if fullmatch(setting["datetime"], now.isoformat()):
                return True

        if "weekday" in setting:
            if str(now.isoweekday()) in str(setting["weekday"]):
                return True

        return False
    else:
        return False


async def start_recording(channel: str) -> bool:
    channel = channel.lower()
    if is_recording(channel):
        return False

    cmd: str = config.get("record/command")
    path: str = config.get("record/filepath")
    if not cmd or not path:
        return False

    now = dt.utcnow().astimezone()
    now += now.utcoffset()

    url = STREAM_URL.format(channel)
    path: Path = get_path(now.strftime(path.format(channel=channel)))
    cmd: str = cmd.format(channel=channel, path=path, url=url)

    path.parent.mkdir(exist_ok=True)
    proc = await create_subprocess_shell(cmd, stdout=DEVNULL, stderr=DEVNULL)
    cb = lambda *_: send("warn", f"Stopped recording: {url}")

    loop = get_running_loop()
    waiting = loop.create_task(proc.wait())
    waiting.add_done_callback(cb)

    recordings[channel] = proc, waiting
    send("info", f"Started recording: {url} -> {path}")
    return True


async def stop_recording(channel: str):
    if channel in recordings:
        try:
            proc, task = recordings[channel]

            if proc:
                proc.kill()
            if task:
                await wait_for(task, 5)
        finally:
            del recordings[channel]
