"""
Module dedicated to coordinating the API, logging, and Output modules
"""

from asyncio import sleep
from datetime import timedelta

from monitor.api import get_page
from monitor.output import dtnow, Page
from monitor.record import is_recording, should_record, start_recording, stop_recording
from util import config
from util.debug import dump, send_later
from util.logs import record_page


class Runner:
    def __init__(self):
        self.latest = {}
        self.previous = {}
        self.page = None

    async def loop(self):
        count = 0
        while True:
            if count <= 0:
                config.refresh()
                await self.update()
                count = config.file.get("refresh", 10)
            if self.page:
                self.page.display()
            dump()
            await sleep(config.file.get("interim", 30))
            count -= 1

    async def update(self):
        """Fetch new information from the API. This will be used for a while."""
        try:
            new = await get_page()
        except Exception as e:
            send_later("warn", "Failed to fetch new data.", e)
            if self.page:
                self.page.refresh = dtnow() + timedelta(
                    seconds=(config.get("interim") * config.get("refresh"))
                )
        else:
            # Only cycle if a new page was successfully fetched
            for channel, data in new.items():
                if isinstance(data, dict) and data.get("stream"):
                    # Channel is live. Does it matter?
                    if should_record(data):
                        # Yes, we should record this. If we are not, start.
                        await start_recording(channel)

                    elif is_recording(channel):
                        # No, but we are. Therefore, stop.
                        await stop_recording(channel)

            self.previous = self.latest
            self.latest = new
            self.page = Page(self.latest, self.previous, self.page)
            record_page(self.page)
