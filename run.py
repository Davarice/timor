#!/usr/bin/env python
"""Timor: Fetch updates from the Twitch API to curate a 'private follow list'.

Options:
  -h, --help                Print this help text
  -c, --config=PATH         Use a different configuration file
  -l, --logfile=PATH        Use a different logfile
  --fallback                Allow falling back to the config logfile if the above fails
  -q, --quiet               Do not perform alert audio playback
"""

from asyncio import get_event_loop
from getopt import GetoptError, getopt
import sys

from monitor.record import kill_all
from util import config, logs, paths
from util.debug import send


cfg = "config.yml"
logfile = None
logfile_fallback = False
relative = sys.argv[0]


try:
    # Validate options and arguments.
    opts, args = getopt(
        sys.argv[1:], "hc:l:q", ["help", "config=", "logfile=", "quiet", "fallback"]
    )
except GetoptError as err:
    # If any are invalid, user gets told off.
    print(str(err).capitalize())
    print(__doc__)
    sys.exit(2)


for opt, arg in opts:
    # Process options.
    if opt in ("--help", "-h"):
        # Help text.
        print(__doc__)
        sys.exit(2)
    elif opt in ("--config", "-c"):
        # Specify custom config.
        cfg = arg
    elif opt in ("--logfile", "-l"):
        # Specify custom config.
        logfile = arg
    elif opt == "--fallback":
        logfile_fallback = True
    elif opt in ("--quiet", "-q"):
        config.sound_suppress = True
    else:
        send("error", "Unknown Option: {}".format(opt))


# Make sure everything initializes correctly.

# Initialize pathing
paths.chroot(relative)

# Initialize and enforce config
config.load(cfg)
config.enforce()

# Initialize logfile
logs.load(logfile, logfile_fallback)

# Initialize main instance
from core import Runner

r = Runner()

# All systems go, start monitoring.
if __name__ == "__main__":
    loop = get_event_loop()
    try:
        loop.run_until_complete(r.loop())
    except KeyboardInterrupt:
        print("* Interrupted.")
    finally:
        loop.run_until_complete(kill_all())
